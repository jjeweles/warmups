package com.galvanize;

public class ArrayIntList {
    private int[] elementData;
    private int size;

    public ArrayIntList() {
        this.elementData = new int[100];
        this.size = 0;
    }

    public void add(int value) {
        this.elementData[this.size] = value;
        this.size++;
    }

    public int get(int index) {
        return this.elementData[index];
    }

    public int size() {
        return this.size;
    }

    public void remove(int index) {
        for (int i = index; i < this.size - 1; i++) {
            this.elementData[i] = this.elementData[i + 1];
        }
        this.size--;
    }

    public void insert(int index, int value) {
        for (int i = this.size; i > index; i--) {
            this.elementData[i] = this.elementData[i - 1];
        }
        this.elementData[index] = value;
        this.size++;
    }

    public String toString() {
        String result = "[";
        for (int i = 0; i < this.size; i++) {
            result += this.elementData[i];
            if (i < this.size - 1) {
                result += ", ";
            }
        }
        result += "]";
        return result;
    }
}