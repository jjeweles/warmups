package com.galvanize;

public class Main {
    public static void main(String[] args) {

        ArrayIntList list = new ArrayIntList();

        for (int i = 0; i < 10; i++) {
            list.add(i);
        }

        System.out.println(list);

        list.insert(0, 100);

        System.out.println(list);

        list.remove(0);

        System.out.println(list);

    }
}