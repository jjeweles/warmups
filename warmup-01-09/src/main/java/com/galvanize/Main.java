package com.galvanize;

//Create a new Java project
//        Create a variable called myString and assign it a String value
//        fCreate a Create function called getMyString that prints the string myString
//        Call that function in Main
//        If you have time:
//        Create a function called setMyString that accepts a string as an argument and sets myString to that argument
//        Call setMyString to change the string value
//        Call getMyString to confirm the change
//        If you have time:
//        Wrap the variables and methods we just wrote in their own class called stringThings
//        Create an instance of stringThings in Main and confirm it works by completing the above again

public class Main {

    public static void main(String[] args) {
        stringThings stringThings = new stringThings("Heres the first string");

        stringThings.getMyString();

        stringThings.setMyString("Heres the changed string");

        stringThings.getMyString();
    }

}