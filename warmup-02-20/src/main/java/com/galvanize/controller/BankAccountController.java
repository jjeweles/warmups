package com.galvanize.controller;

import com.galvanize.model.BankAccount;
import org.apache.tomcat.util.json.JSONParser;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/api/v1/bankaccount")
public class BankAccountController {

    BankAccount bankAccount;

    List<BankAccount> bankAccounts = new ArrayList<>(Arrays.asList(
            new BankAccount(1L, "John", 100.00f),
            new BankAccount(2L, "Jane", 200.00f),
            new BankAccount(3L, "Jack", 300.00f)
    ));

    @GetMapping("{id}")
    public String getBankAccountById(@PathVariable Long id) {
        return bankAccounts.get(id.intValue() - 1).toString();
    }

    @PostMapping("add")
    public String addBankAccount(@RequestBody BankAccount newBankAccount) {
        bankAccounts.add(newBankAccount);
        return "bankAccount = " + newBankAccount;
    }

    @PutMapping("update/{id}")
    public String updateBankAccount(@PathVariable String id, @RequestBody String newBankAccount) {
        return "id = " + id + ", bankAccount = " + newBankAccount;
    }

    @DeleteMapping("{id}")
    public String deleteBankAccount(@PathVariable String id) {
        return "id = " + id;
    }

}
