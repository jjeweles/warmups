package com.galvanize;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Warmup0220Application {

    public static void main(String[] args) {
        SpringApplication.run(Warmup0220Application.class, args);
    }

}
