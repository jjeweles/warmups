package com.galvanize;

public class Main {
    public static void main(String[] args) {

        // declare var and print to screen
        int myNumber = 27;
        System.out.println("My Number: " + myNumber);
        // loop over num and print to screen
        for (int i = 0; i <= myNumber; i++) {
            System.out.println(i);
        }

        // print num between 10 and myNumber adding 3 each time
        int newNum = 0;
        while (newNum <= myNumber) {
            System.out.println(newNum);
            newNum += 3;
        }
    }
}