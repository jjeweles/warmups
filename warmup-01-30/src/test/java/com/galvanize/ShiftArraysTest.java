package com.galvanize;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShiftArraysTest {

    @Test
    void shiftArrayNumsShouldReturnArrayListedShiftedToRightByPassedArgument() {
        // Setup
        ShiftArrays shiftArrays = new ShiftArrays();
        ArrayList<Integer> arr = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6));
        // Enact
        shiftArrays.shiftArrayNums(arr, 2);
        // Assert
        assertEquals("[5, 6, 1, 2, 3, 4]", shiftArrays.shiftArrayNums(arr, 2).toString());

        // Enact
        shiftArrays.shiftArrayNums(arr, 2);
        // Assert
        assertEquals("[3, 4, 5, 6, 1, 2]", shiftArrays.shiftArrayNums(arr, 2).toString());
    }

    @Test
    void shiftArrayStringsShouldReturnArrayListedShiftToRightByPassedArgument() {
        // Setup
        ShiftArrays shiftArrays = new ShiftArrays();
        ArrayList<String> arr = new ArrayList<String>(Arrays.asList("I'm", "The", "Problem", "It\'s", "Me"));
        // Enact
        shiftArrays.shiftArrayStrings(arr, 2);
        // Assert
        assertEquals("[It's, Me, I'm, The, Problem]", shiftArrays.shiftArrayStrings(arr, 2).toString());
    }

    @Test
    void shiftArrayShouldTakeEitherAnIntOrStringArrayAndShift() {
        // Setup
        ShiftArrays shiftArrays = new ShiftArrays();
        ArrayList<String> arr = new ArrayList<String>(Arrays.asList("I'm", "The", "Problem", "It\'s", "Me"));
        ArrayList<Integer> arr2 = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6));

        // Enact
        shiftArrays.shiftArray(arr, 2);
        // Assert
        assertEquals("[It's, Me, I'm, The, Problem]", shiftArrays.shiftArray(arr, 2).toString());

        // Enact
        shiftArrays.shiftArray(arr2, 2);
        // Assert
        assertEquals("[5, 6, 1, 2, 3, 4]", shiftArrays.shiftArray(arr2, 2).toString());

        // Enact
        shiftArrays.shiftArray(arr2, 2);
        // Assert
        assertEquals("[3, 4, 5, 6, 1, 2]", shiftArrays.shiftArray(arr2, 2).toString());
    }

//    @Test
//    void shiftedArrayShouldReturnShiftedArray() {
//        // Setup
//        ShiftArrays shiftArrays = new ShiftArrays();
//        ArrayList<Object> arr = new ArrayList<Object>(Arrays.asList(1, 2, 3, 4, 5, 6));
//        // Assert
//        assertEquals("[5, 6, 1, 2, 3, 4]", shiftArrays.shiftedArray(arr, 2).toString());
//    }

}
