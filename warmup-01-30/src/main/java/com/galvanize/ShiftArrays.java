package com.galvanize;

import java.util.ArrayList;

public class ShiftArrays {

    public ArrayList<Integer> shiftArrayNums(ArrayList<Integer> arr, int shift) {
        for (int i = 0; i < shift - 1; i++) {
            int temp = arr.get(arr.size() - 1);
            for (int j = arr.size() - 1; j > 0; j--) {
                arr.set(j, arr.get(j - 1));
            }
            arr.set(0, temp);
        }

        return arr;
    };

    public ArrayList<String> shiftArrayStrings(ArrayList<String> arr, int shift) {
        for (int i = 0; i < shift - 1; i++) {
            String temp = arr.get(arr.size() - 1);
            for (int j = arr.size() - 1; j > 0; j--) {
                arr.set(j, arr.get(j - 1));
            }
            arr.set(0, temp);
            arr.add(0, temp);
        }
        return arr;
    };

    public static Object shiftArray(ArrayList arr, int shift) {
        if (arr.get(0).getClass().getName() == "java.lang.Integer") {
            for (int i = 0; i < shift - 1; i++) {
                int temp = (int) arr.get(arr.size() - 1);
                for (int j = arr.size() - 1; j > 0; j--) {
                    arr.set(j, arr.get(j - 1));
                }
                arr.set(0, temp);
            }
        } else if (arr.get(0).getClass().getName() == "java.lang.String") {
            for (int i = 0; i < shift - 1; i++) {
                String temp = (String) arr.get(arr.size() - 1);
                for (int j = arr.size() - 1; j > 0; j--) {
                    arr.set(j, arr.get(j - 1));
                }
                arr.set(0, temp);
            }
        }

        return arr;
    }

//    public ArrayList<Object> shiftedArray(ArrayList<Object> arr, int shift){
//        ArrayList<Object> shiftedArr = new ArrayList<Object>();
//
//        int numObjects = arr.size();
//
//        for (int i = 0; i < numObjects; i++) {
//
//            if (i + shift < numObjects) {
//                shiftedArr.add(arr.get(i + shift));
//            } else {
//                shiftedArr.add(arr.get((i + shift) % numObjects));
//            }
//
//        }
//
//        return shiftedArr;
//    }

}
