package com.galvanize;

public class Clothes {

    private boolean clean;
    private boolean dryCleanOnly;

    public boolean isDryCleanOnly() {
        return dryCleanOnly;
    }

    public void setDryCleanOnly(boolean dryCleanOnly) {
        this.dryCleanOnly = dryCleanOnly;
    }

    void wear() {
        clean = false;
    }

    void wash() {
        clean = true;
    }

    public boolean isClean() {
        return clean;
    }
}
