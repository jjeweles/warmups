package com.galvanize;

public class Pants extends Clothes{

    private boolean flyDown;

    void zip() {
        flyDown = false;
    }

    void unZip() {
        flyDown = true;
    }

    void useZipper() {
        flyDown = !flyDown;
    }

    public boolean isFlyDown() {
        return flyDown;
    }
}
