package com.galvanize;

public class Shirt extends Clothes {
    private boolean buttoned;

    public boolean isButtoned() {
        return buttoned;
    }

    public void setButtoned(boolean buttoned) {
        if (buttoned) {
            setDryCleanOnly(true);
        }
        this.buttoned = buttoned;
    }
}
