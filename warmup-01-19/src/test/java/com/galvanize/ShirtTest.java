package com.galvanize;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShirtTest {

    @Test
    void shouldReturnBooleanValueTrueOrFalseIfShirtIsButtoned() {
        // Setup
        Shirt shirt = new Shirt();

        // Enact
        shirt.isButtoned();

        // Assert
        assertEquals(false, shirt.isButtoned());

        // Enact
        shirt.setButtoned(true);

        // Assert
        assertEquals(true, shirt.isButtoned());

    }

    @Test
    void isDryCleanOnlyShouldReturnTrueIfShirtIsButtonedAndFalseIfShirtIsNotButtoned() {
        // Setup
        Shirt shirt = new Shirt();

        // Enact
        shirt.setButtoned(false);

        // Assert
        assertEquals(false, shirt.isDryCleanOnly());

        // Enact
        shirt.setButtoned(true);

        // Assert
        assertEquals(true, shirt.isDryCleanOnly());




    }

}
