package com.galvanize;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PantsTest {

    @Test
    void shouldReturnValueOfCleanAndFlyDownBooleans() {
        // Setup
        Pants pants = new Pants();

        // Enact
        pants.isClean();
        pants.isFlyDown();

        // Assert
        assertEquals(false, pants.isClean());
        assertEquals(false, pants.isFlyDown());

    }

    @Test
    void methodsShouldChangeBooleanValuesOfCleanAndFlyDownWhenCalled() {
        // Setup
        Pants pants = new Pants();

        // Enact
        pants.wash(); // true
        // Assert
        assertEquals(true, pants.isClean());
        // Enact
        pants.wear(); // false
        // Assert
        assertEquals(false, pants.isClean());
        // Enact
        pants.unZip(); // true
        // Assert
        assertEquals(true, pants.isFlyDown());
        // Enact
        pants.zip();
        // Assert
        assertEquals(false, pants.isFlyDown());

        // Enact
        pants.useZipper(); // flip whatever boolean value is
        // Assert
        assertEquals(true, pants.isFlyDown()); // zipper was false before useZipper, should be true now
    }

}
