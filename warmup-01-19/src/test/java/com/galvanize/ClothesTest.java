package com.galvanize;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ClothesTest {

    @Test
    void shouldReturnTrueIfClothesAreDryCleanOnly() {
        // Setup
        Clothes clothes = new Clothes();

        // Enact
        clothes.isDryCleanOnly();
        // Assert
        assertEquals(false, clothes.isDryCleanOnly());
        // Enact - change dry clean only to true
        clothes.setDryCleanOnly(true);
        // Assert
        assertEquals(true, clothes.isDryCleanOnly());
    }


}
