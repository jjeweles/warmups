package com.galvanize.warmup0224;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Warmup0224Application {

    public static void main(String[] args) {
        SpringApplication.run(Warmup0224Application.class, args);
    }

}
