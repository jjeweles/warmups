package com.galvanize.warmup0224;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping("/api/automobiles")
public class AutoController {

    HashMap<Integer, Automobile> autoMap = new HashMap<>();

    @PostMapping("seed")
    public void seedAutomobiles() {
        autoMap.put(1, new Automobile("Ford", "F150", 2020));
        autoMap.put(2, new Automobile("Chevy", "Silverado", 2020));
        autoMap.put(3, new Automobile("Toyota", "Tacoma", 2020));
        autoMap.put(4, new Automobile("Honda", "Civic", 2020));
        autoMap.put(5, new Automobile("Nissan", "Altima", 2020));
        autoMap.put(6, new Automobile("Ford", "Mustang", 2020));
        autoMap.put(7, new Automobile("Chevy", "Camaro", 2020));
        autoMap.put(8, new Automobile("Toyota", "Corolla", 2020));
        autoMap.put(9, new Automobile("Honda", "Accord", 2020));
        autoMap.put(10, new Automobile("Nissan", "Sentra", 2020));
    }

    @GetMapping("")
    public HashMap<Integer, Automobile> getAllAutomobiles() {
        return autoMap;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Automobile> getAutomobileById(@PathVariable int id) {
        Automobile auto = autoMap.get(id);
        return auto == null ? ResponseEntity.noContent().build() : ResponseEntity.ok(auto);
    }

    @PostMapping("add")
    public String addAutomobile(@RequestBody Automobile automobile) {
        int id = autoMap.size() + 1;
        autoMap.put(id, automobile);
        return "Automobile added successfully";
    }

    @PutMapping("update/{id}")
    public String updateAutomobile(@PathVariable int id, @RequestBody Automobile automobile) {
        autoMap.put(id, automobile);
        return "Automobile updated successfully";
    }

    @DeleteMapping("delete/{id}")
    public String deleteAutomobile(@PathVariable int id) {
        autoMap.remove(id);
        return "Automobile deleted successfully";
    }
}
