package com.galvanize;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class InventoryTest {

    Inventory inventory1;

    @BeforeEach
    void setUp() throws Exception {
        inventory1 = new Inventory();

        inventory1.addThings("guns", 1);
        inventory1.addThings("guns", 1);
        inventory1.addThings("guns", 1);
        inventory1.addThings("guns", 1);

        inventory1.addThings("paper", 24);
        inventory1.addThings("paper", 76);

        inventory1.addThings("money", 2323);
    }

    @Test
    void addThingsAddsAKeyAndValueToHashMapAndUpdatesIntQuantiyIfKeyExists() {
        HashMap<String, Integer> testHashMap = new HashMap<>();
        testHashMap.put("guns", 4);
        testHashMap.put("paper", 100);
        testHashMap.put("money", 2323);
        
        assertEquals(testHashMap, inventory1.getInventory());
    }

    @Test
    void addThingsShouldThrowExceptionIfQuantityIsNegative() {

        assertThrows(Exception.class, () -> inventory1.addThings("paper", -100));
    }

    @Test
    void removeThingsDecreasesQuantityIntegerWhenPassedArgumentsNameAndValueThatExistInHashMap() throws Exception {
        HashMap<String, Integer> testHashMap = new HashMap<>();
        testHashMap.put("guns", 4);
        testHashMap.put("paper", 76);
        testHashMap.put("money", 2323);

        inventory1.removeThings("paper", 24);

        assertEquals(testHashMap, inventory1.getInventory());
    }

    @Test
    void emptyInventoryClearsTheHashMap() {
        HashMap<String, Integer> testHashMap = new HashMap<>();

        inventory1.emptyInventory();

        assertEquals(testHashMap, inventory1.getInventory());
    }


}