package com.galvanize;

import java.util.HashMap;

public class Inventory {

    HashMap<String, Integer> inventory = new HashMap<>();

    void addThings(String name, int quantity) throws Exception {
        try {
            if (quantity < 0) {
                throw new Exception("Quantity cannot be negative.");
            }
            int tempQuantity;
            if (inventory.containsKey(name)) {
                tempQuantity = inventory.get(name);
                inventory.put(name, (tempQuantity + quantity));
            } else {
                inventory.put(name, quantity);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            throw (e);
        }
    }

    void removeThings(String name, int quantity) throws Exception {
        try {
            if (quantity < 0) {
                throw new Exception("Quantity cannot be negative.");
            }
            int tempQuantity;
            if (inventory.containsKey(name)) {
                tempQuantity = inventory.get(name);
                inventory.put(name, (tempQuantity - quantity));
            } else {
                inventory.put(name, quantity);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            throw (e);
        }
    }

    void emptyInventory() {
        try {
            inventory.clear();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public HashMap<String, Integer> getInventory() {
        return inventory;
    }

}
