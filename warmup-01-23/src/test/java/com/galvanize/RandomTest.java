package com.galvanize;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class RandomTest {


    @Test
    void sumTotalShouldReturnSumOfAllIntegersInArrayList(){
        // Setup
        RandomArray rand = new RandomArray();
        ArrayList<Integer> myArray = new ArrayList<>(Arrays.asList(1,2,3,4,5));

        // Enact
        // rand.sumTotal(myArray);

        // Assert
        assertEquals(15, rand.sumTotal(myArray));
        // E
        myArray.add(5);
        // A
        assertEquals(20, rand.sumTotal(myArray));
        // E & A
        myArray.add(80);
        assertEquals(100, rand.sumTotal(myArray));

    }

    @Test
    void isSortedShouldReturnTrueIfElsInArrayListAreInAscendingOrderAndFalseIfNot() {
        // Setup
        RandomArray rand = new RandomArray();
        ArrayList<Integer> myArray = new ArrayList<>(Arrays.asList(1,2,3,4,5));
        ArrayList<Integer> myArray2 = new ArrayList<>(Arrays.asList(5,4,3,2,1));
        ArrayList<Integer> myArray3 = new ArrayList<>(Arrays.asList(1,3,2,4,5));
        ArrayList<Integer> myArray4 = new ArrayList<>(Arrays.asList(1,2,3,80,2));

        // Enact
        //rand.isSorted(myArray);

        // Assert
        assertEquals(true, rand.isSorted(myArray));
        assertEquals(false, rand.isSorted(myArray2));
        assertEquals(false, rand.isSorted(myArray3));
        assertEquals(false, rand.isSorted(myArray4));

    }

    @Test
    void isGreaterThanShouldReturnTheTotalNumberOfElsInArrayThatAreGreaterThanPassedInArgumentInt() {
        // Setup
        RandomArray rand = new RandomArray();
        ArrayList<Integer> myArray = new ArrayList<>(Arrays.asList(1,2,3,4,5));

        // Enact method
        // rand.isGreaterThan(myArray, 10);

        // Assert
        assertEquals(5, rand.isGreaterThan(myArray, 0));
        assertEquals(0, rand.isGreaterThan(myArray, 5));
        assertEquals(2, rand.isGreaterThan(myArray, 3));

    }

    @Test
    void hasDoubleShouldReturnTrueIfArrayListHasMultipleCharacters() {
        // Setup
        RandomArray rand = new RandomArray();
        ArrayList<Integer> myArray = new ArrayList<>(Arrays.asList(1,2,3,4,5));

        // Enact method
        // rand.hasDouble(myArray);

        // Assert
        assertFalse(rand.hasDouble(myArray));

        myArray.add(4);
        assertTrue(rand.hasDouble(myArray));

        myArray.add(2);
        assertTrue(rand.hasDouble(myArray));

    }

}
