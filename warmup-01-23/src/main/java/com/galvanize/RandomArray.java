package com.galvanize;

import java.util.ArrayList;
import java.util.Collections;
@SuppressWarnings("unchecked")

public class RandomArray {


    public int sumTotal(ArrayList<Integer> arr) {
        int sum = 0;

        for (Integer n : arr) {
            sum += n;
        }

        return sum;
    }

    public boolean isSorted(ArrayList<Integer> arr) {
        boolean sorted = false;
        ArrayList<Integer> arrClone = (ArrayList<Integer>) arr.clone();
        Collections.sort(arrClone);

        if (arrClone.equals(arr)) {
            sorted = true;
        }

        /* for (int i = 1; i < arr.size(); i++) {
            if (tempN < arr.get(i)) {
                sorted = true;
            } else {
                sorted = false;
                break;
            }
            tempN = arr.get(i);
        }*/

        return sorted;
    }

    public int isGreaterThan(ArrayList<Integer> arr, int numGreaterThan){
        int numGreater = 0;

        for (Integer els : arr) {
            if (numGreaterThan < els) {
                numGreater += 1;
            }
        }

        return numGreater;
    }

    public boolean hasDouble(ArrayList<Integer> arr){
        boolean hasDouble = false;

        for (Integer els : arr) {
            if (Collections.frequency(arr, els) > 1) {
                hasDouble = true;
                break;
            }
        }

        return hasDouble;
    }

}
