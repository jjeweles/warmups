package com.galvanize;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Warmup {

    public ArrayList<HashMap<String, List<Integer>>> myPeople = new ArrayList<>();
    public ArrayList<Integer> newList = new ArrayList<>();


    ArrayList<Integer> sortArray(ArrayList<Integer> arr) {
        for (int i = 0; i < arr.size() - 1; i++)
        {
            for (int j = i + 1; j < arr.size(); j++)
            {
                if (arr.get(i) > arr.get(j)) {
                    int temp = arr.get(j);
                    arr.set(j, arr.get(i));
                    arr.set(i, temp);
                }
            }
        }
        return arr;
    }

    ArrayList<HashMap<String, List<Integer>>> addHashMapToArray(String name, Integer age) {
        HashMap<String, List<Integer>> nameAndAgeHash = new HashMap<>();
        List<Integer> ageList = new ArrayList<>();

        if (name.equals("") || age == null) {
            return myPeople;
        }

        for (HashMap<String, List<Integer>> el : newList) {
            if (el.containsKey(name)) {
                ageList = (ArrayList<Integer>) el.get(name);
                ageList.add(age);
                el.put(name, ageList);
                return myPeople;
            }
        }

        ageList.add(age);
        nameAndAgeHash.put(name, ageList);
        myPeople.add(nameAndAgeHash);

        return myPeople;
    }

}
