package com.galvanize;

import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

/*
Write a method called whoAreMyPeople that accepts an ArrayList of HashMaps
as an argument. It should then return an ArrayList of Strings that
read "I know X number of name  and they are age and age years old"
For bonus points make it grammatically correct
 */

public class WarmupTest {

    @Test
    void sortArrayShouldReturnASortedArrayList() {
        Warmup warmup = new Warmup();
        ArrayList<Integer> arrOfInts = new ArrayList<>();
        arrOfInts.add(10);
        arrOfInts.add(15);
        arrOfInts.add(3);
        arrOfInts.add(1);
        arrOfInts.add(2);
        ArrayList<Integer> sortArr = new ArrayList<>(Arrays.asList(1,2,3,10,15));

        // Enact
        warmup.sortArray(arrOfInts);

        assertEquals(sortArr, warmup.sortArray(arrOfInts));
    }

    @Test
    void foo() {
        Warmup warmup = new Warmup();
        HashMap<String, List<Integer>> newHash = new HashMap<>();
        HashMap<String, List<Integer>> newHash2 = new HashMap<>();
        HashMap<String, List<Integer>> newHash3 = new HashMap<>();
        HashMap<String, List<Integer>> newHash4 = new HashMap<>();
        ArrayList<HashMap<String, List<Integer>>> myPeople = new ArrayList<>();
        newHash.put("Justin", Collections.singletonList(36));
        newHash2.put("Jeff", Collections.singletonList(22));
        newHash3.put("Mike", Collections.singletonList(56));
        newHash4.put("Mike", Collections.singletonList(76));
        myPeople.add(newHash);
        myPeople.add(newHash2);
        myPeople.add(newHash3);
        myPeople.add(newHash4);

        warmup.addHashMapToArray("Justin", 36);
        warmup.addHashMapToArray("Jeff", 22);
        warmup.addHashMapToArray("Mike", 56);
        warmup.addHashMapToArray("Mike", 76);

        System.out.println(myPeople);
        System.out.println(warmup.myPeople);

//        assertEquals(myPeople, warmup.myPeople);
    }
}
