package com.galvanize;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ClassPetsTest {

    ClassPets classPetsTest;

    @BeforeEach
    void setUp() {
        classPetsTest = new ClassPets();
    }

    @Test
    void addToCollectionShouldTakeInHashMapAndAddToClassPets() {
        HashMap<String, List<String>> classPetsArg = new HashMap<>();
        ArrayList<String> pets = new ArrayList<>();
        pets.add("Buddy");
        pets.add("Spot");
        classPetsArg.put("Andy", pets);
        ArrayList<String> pets2 = new ArrayList<>();
        pets2.add("Fido");
        pets2.add("Rover");
        classPetsArg.put("Brett", pets2);
        classPetsTest.addToCollection(classPetsArg);
        // add pets to Bretts list
        ArrayList<String> pets3 = new ArrayList<>();
        pets3.add("Spot");
        classPetsArg.put("Brett", pets3);
        classPetsTest.addToCollection(classPetsArg);
        // assert
        HashMap<String, List<String>> expected = new HashMap<>();
        expected.put("Andy", Arrays.asList("Buddy", "Spot"));
        expected.put("Brett", Arrays.asList("Fido", "Rover", "Spot"));
        HashMap<String, List<String>> actual = classPetsTest.getClassPets();
        assertEquals(expected, actual);
    }

    @Test
    void formatPetsShouldReturnStringWithNoPets() {
        String name = "Andy";
        String expected = "Andy has no pets.";
        String actual = classPetsTest.formatPets(name);
        assertEquals(expected, actual);
    }

    @Test
    void formatPetsShouldReturnStringWithPets() {
        HashMap<String, List<String>> classPetsArg = new HashMap<>();
        classPetsArg.put("Andy", Arrays.asList("Buddy", "Spot"));
        classPetsArg.put("Brett", Arrays.asList("Fido", "Rover"));
        classPetsArg.put("Jeffrey", Arrays.asList("Fido", "Rover", "Spot"));
        classPetsArg.put("John", List.of("Rover"));
        classPetsTest.addToCollection(classPetsArg);

        String name = "Andy";
        String expected = "Andy has 2 pets named Buddy and Spot.";
        String actual = classPetsTest.formatPets(name);

        assertEquals(expected, actual);

        name = "Brett";
        expected = "Brett has 2 pets named Fido and Rover.";
        actual = classPetsTest.formatPets(name);

        assertEquals(expected, actual);

        name = "John";
        expected = "John has a pet named Rover.";
        actual = classPetsTest.formatPets(name);

        assertEquals(expected, actual);

        name = "Jeffrey";
        expected = "Jeffrey has 3 pets named Fido, Rover and Spot.";
        actual = classPetsTest.formatPets(name);

        assertEquals(expected, actual);

    }

    @Test
    void formatPetsShouldReturnStringWithAllPets() {
        HashMap<String, List<String>> classPetsArg = new HashMap<>();
        classPetsArg.put("Andy", Arrays.asList("Buddy", "Spot"));
        classPetsArg.put("Brett", Arrays.asList("Fido", "Rover"));
        classPetsTest.addToCollection(classPetsArg);

        String expected = "[[Fido, Rover], [Buddy, Spot]]";
        // String expected = "[[Buddy, Spot], [Fido, Rover]]";
        String actual = classPetsTest.formatPets();

        assertEquals(expected, actual);
    }

}
