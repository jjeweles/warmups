package com.galvanize;

import java.util.HashMap;
import java.util.List;

public class ClassPets {

    private final HashMap<String, List<String>> classPets = new HashMap<>();

    public void addToCollection(HashMap<String, List<String>> classPetsArg) {
        for (String key : classPetsArg.keySet()) {
            if (classPets.containsKey(key)) {
                if (classPets.get(key).equals(classPetsArg.get(key))) {
                    continue;
                }
                classPets.get(key).addAll(classPetsArg.get(key));
            } else {
                classPets.put(key, classPetsArg.get(key));
            }
        }
    }

    public String formatPets(String name) {
        String petsFormat;
        if (!classPets.containsKey(name)) {
            return name + " has no pets.";
        } else {
            List<String> pets = classPets.get(name);
            if (pets.size() == 1) {
                petsFormat = name + " has a pet named " + pets.get(0) + ".";
            } else {
                petsFormat = name + " has " + pets.size() + " pets named " + pets.get(0);
                for (int i = 1; i < pets.size() - 1; i++) {
                    petsFormat += ", " + pets.get(i);
                }
                petsFormat += " and " + pets.get(pets.size() - 1) + ".";
            }
            return petsFormat;
        }
    }

    public String formatPets() {
        String petsFormat = "";
        for (String ignored : classPets.keySet()) {
            petsFormat = classPets.values().toString();
        }
        return petsFormat;
    }

    public HashMap<String, List<String>> getClassPets() {
        return classPets;
    }
}
