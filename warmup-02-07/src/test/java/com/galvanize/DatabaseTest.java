package com.galvanize;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DatabaseTest {

    Database dbTestInstance;
    HashMap<Integer, String> dbTestHashMap;

    @BeforeEach
    void setUp() {
        System.out.println("\nCreate table in DB: ");
        dbTestInstance = new Database();
        dbTestHashMap = new HashMap<>();

        dbTestHashMap.put(1, "one");
        dbTestHashMap.put(2, "two");
        dbTestHashMap.put(3, "three");

        System.out.println("Insert records into DB: ");
        dbTestInstance.create(dbTestHashMap);
    }

    @Test
    void createShouldAddRecordsToDBOrReturnStringIfHashMapIsEmpty() {
        assertEquals("{1=one, 2=two, 3=three}", dbTestInstance.getDatabase().toString());
    }

    @Test
    void readShouldReturnRecordFromDB() {
        System.out.println("Select Passed Key/Value from Database: ");
        String actual = dbTestInstance.read(2, "1");
        assertEquals("two", actual);
    }

    @Test
    void readShouldReturnAllDBRecordsIfStarIsPassed() {
        System.out.println("Select All Records from Database: ");
        String actual = dbTestInstance.read(2, "*");
        assertEquals("{1=one, 2=two, 3=three}", actual);
    }

    @Test
    void updateShouldUpdateRecordInDBAndReturnUpdatedDB() {
        System.out.println("Update Passed Key/Value in Database: ");
        dbTestInstance.update(2, "TWO");

        assertEquals("{1=one, 2=TWO, 3=three}", dbTestInstance.getDatabase().toString());
    }

    @Test
    void deleteShouldDeleteRecordFromDBAndReturnUpdatedDB() {
        System.out.println("Delete Passed Key/Value from Database: ");
        dbTestInstance.delete(2, "1");

        assertEquals("{1=one, 3=three}", dbTestInstance.getDatabase().toString());
    }

    @Test
    void deleteShouldDeleteAllRecordsFromDBIfStarIsPassed() {
        System.out.println("Delete All Records from Database: ");
        dbTestInstance.delete(2, "*");

        assertEquals("{}", dbTestInstance.getDatabase().toString());
    }

    @Test
    void swingPopupWithAllSQLQueriesShouldDisplayAllSQLQueriesInPopupWindow() {
        dbTestInstance.swingPopupWithAllSQLQueries();
    }

}
