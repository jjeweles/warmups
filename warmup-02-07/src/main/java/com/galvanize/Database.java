package com.galvanize;

import java.util.HashMap;
import javax.swing.*;

public class Database {

    private HashMap<Integer, String> database = new HashMap<>();

    public Database() {
        System.out.println("CREATE TABLE database (key INT, value VARCHAR(255)");
    }

    public Database(HashMap<Integer, String> database) {
        this.database = database;
    }

    public void create(HashMap<Integer, String> dbEntry) {
        System.out.println("INSERT INTO database (key, value) VALUES (" + dbEntry.keySet() + ", " + dbEntry.values() + ")");
        database.putAll(dbEntry);
    }

    public String read(int key, String star) {
        if (!database.containsKey(key)) {
            return "Key does not exist in database";
        }
        if (star.equals("*")) {
            System.out.println("SELECT * FROM database");
            return database.toString();
        }
        System.out.println("SELECT * FROM database WHERE key = " + key);
        return database.get(key);
    }

    public HashMap<Integer, String> update(int key, String value) {
        if (!database.containsKey(key)) {
            System.out.println("Key does not exist in database");
            return database;
        }
        System.out.println("UPDATE database SET value = " + value + " WHERE key = " + key + "\n");
        database.put(key, value);
        return database;
    }

    public HashMap<Integer, String> delete(int key, String star) {
        if (!database.containsKey(key)) {
            System.out.println("Key does not exist in database");
            return database;
        }
        if (star.equals("*")) {
            System.out.println("DELETE FROM database");
            database.clear();
            return database;
        }
        System.out.println("DELETE FROM database WHERE key = " + key);
        database.remove(key);
        return database;
    }

    public HashMap<Integer, String> getDatabase() {
        return database;
    }

    public void swingPopupWithAllSQLQueries() {
        String sqlQueries = "CREATE TABLE database (key INT, value VARCHAR(255)\n" +
                "INSERT INTO database (key, value) VALUES (" + database.keySet() + ", " + database.values() + ")\n" +
                "SELECT * FROM database\n" +
                "SELECT * FROM database WHERE key = " + database.keySet() + "\n" +
                "UPDATE database SET value = " + database.values() + " WHERE key = " + database.keySet() + "\n" +
                "DELETE FROM database\n" +
                "DELETE FROM database WHERE key = " + database.keySet();
        JOptionPane.showMessageDialog(null, sqlQueries);
    }
}
