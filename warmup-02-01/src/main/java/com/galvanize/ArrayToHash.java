package com.galvanize;

import java.util.ArrayList;
import java.util.HashMap;

public class ArrayToHash {

    public HashMap<Integer, String> arrayToHashMap(ArrayList<String> arr) {
        HashMap<Integer, String> map = new HashMap<>();
        for (int i = 0; i < arr.size(); i++) {
            map.put(i, arr.get(i));
        }
        System.out.println(map);
        return map;
    }

    public HashMap<Integer, String> arrayToHashMap2(String[] arr) {
        HashMap<Integer, String> map = new HashMap<>();
        for (int i = 0; i < arr.length; i++) {
            map.put(i, arr[i]);
        }
        System.out.println(map);
        return map;
    }

}
