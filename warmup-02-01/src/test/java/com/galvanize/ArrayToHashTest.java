package com.galvanize;

import org.junit.jupiter.api.*;
import java.util.*;
import static org.junit.jupiter.api.Assertions.*;

public class ArrayToHashTest {

    HashMap<Integer, String> map;
    HashMap<Integer, String> map2;

    @BeforeEach
    void setUp() {
        map = new HashMap<>();
        map.put(0, "Shake");
        map.put(1, "It");
        map.put(2, "Off");

        map2 = new HashMap<>();
        map2.put(0, "Down");
        map2.put(1, "Here");
        map2.put(2, "At");
        map2.put(3, "The");
        map2.put(4, "Pawn");
        map2.put(5, "Shop");
    }

    @Test
    void arrayToHashMapShouldReturnPassedArrayListAsHashMapWithKeyBeingIndexAndValueBeingValue() {
        ArrayToHash arrayToHash = new ArrayToHash();
        ArrayList<String> arr = new ArrayList<>();
        arr.add("Shake");
        arr.add("It");
        arr.add("Off");

        assertEquals(map, arrayToHash.arrayToHashMap(arr));

        ArrayList<String> arr2 = new ArrayList<>();
        arr2.add("Down");
        arr2.add("Here");
        arr2.add("At");
        arr2.add("The");
        arr2.add("Pawn");
        arr2.add("Shop");

        assertEquals(map2, arrayToHash.arrayToHashMap(arr2));

    }

    @Test
    void arrayToHashMap2ShouldReturnPassedArrayAsHashMapWithKeyBeingIndexAndValueBeingValue() {
        ArrayToHash arrayToHash = new ArrayToHash();
        String[] arr = {"Shake", "It", "Off"};

        assertEquals(map, arrayToHash.arrayToHashMap2(arr));

        String[] arr2 = {"Down", "Here", "At", "The", "Pawn", "Shop"};

        assertEquals(map2, arrayToHash.arrayToHashMap2(arr2));

    }

}
