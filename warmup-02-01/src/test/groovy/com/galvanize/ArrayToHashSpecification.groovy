package com.galvanize

import spock.lang.Specification


class ArrayToHashSpecification extends Specification {

    def "array to hash map should return passed info as key and value"() {
        given:
        def arrayToHash = new ArrayToHash()
        def arr = new ArrayList()
        arr.add("Shake")
        arr.add("It")
        arr.add("Off")
        def expected = new HashMap()
        expected.put(0, "Shake")
        expected.put(1, "It")
        expected.put(2, "Off")

        when:
        def actual = arrayToHash.arrayToHashMap(arr)

        then:
        actual == expected
    }
}