package com.galvanize;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    // store list of houses and characters
    static ArrayList<Character> characterList = new ArrayList<Character>();
    static ArrayList<House> houseList = new ArrayList<House>();

    public static void main(String[] args) {
        // start with one character
        Character jonSnow = new Character("Jon Snow", "male", "Lord Commander of the Night's Watch");
        // and start with one house
        House stark = new House("House Stark of Winterfell", "The North", "A running grey direwolf, on an ice-white field");

        // set character allegiance to house and add to sworn members of house
        jonSnow.setAllegiance(stark.getName());
        stark.setSwornMembers(jonSnow.getName());

        // just print out objects to make sure it worked
        System.out.println(jonSnow);
        System.out.println(stark);

        // add Objects to array list
        characterList.add(jonSnow);
        houseList.add(stark);

        // display menu method to add more characters or houses
        displayMenu();

    } // end method main


    public static void displayMenu() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("|------------------------------|");
        System.out.println("| 1. Add A New House           |");
        System.out.println("| 2. Add A New Character       |");
        System.out.println("| 3. Swear Character to House  |");
        System.out.println("| 4. Print Out Houses          |");
        System.out.println("| 5. Print Out Characters      |");
        System.out.println("| 0. Exit                      |");
        System.out.println("|------------------------------|");

        int option;
        try {
            do {
                System.out.print("\nSelect An Option: ");
                option = scanner.nextInt();
                scanner.nextLine();

                switch (option) {
                    case 1:
                        System.out.print("Enter House Name: ");
                        String houseName = scanner.nextLine();
                        System.out.print("Enter House Region: ");
                        String houseRegion = scanner.nextLine();
                        System.out.print("Enter House Coat Of Arms: ");
                        String houseCoA = scanner.nextLine();

                        House house = new House(houseName, houseRegion, houseCoA);
                        houseList.add(house);
                        break;
                    case 2:
                        System.out.print("Enter Character Name: ");
                        String charName = scanner.nextLine();
                        System.out.print("Enter Character Gender: ");
                        String charGender = scanner.nextLine();
                        System.out.print("Enter Character Title: ");
                        String charTitle = scanner.nextLine();

                        Character character = new Character(charName, charGender, charTitle);
                        characterList.add(character);
                        break;
                    case 3:
                        System.out.println("Under Construction");
                        break;
                    case 4:
                        System.out.println(houseList);
                        break;
                    case 5:
                        System.out.println(characterList);
                        break;
                    case 0:
                        System.out.println("Thank You, Program Ended");
                        scanner.close();
                        break;
                    default:
                        System.out.println("Invalid Input. Please Try Again");
                } // end switch
            } while (option != 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
} // end class Main