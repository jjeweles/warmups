package com.galvanize;

public class Character extends SharedValues {
    private String gender;
    private String title;
    private String allegiance;

    public Character(String name, String gender, String title) {
        this.name = name;
        this.gender = gender;
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAllegience() {
        return allegiance;
    }

    public void setAllegiance(String allegiance) {
        this.allegiance = allegiance;
    }

    @Override
    public String toString() {
        return " Character {" +
                "name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", title='" + title + '\'' +
                ", allegiance=" + allegiance +
                "} ";
    }
}
