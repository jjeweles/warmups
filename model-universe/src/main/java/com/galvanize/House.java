package com.galvanize;

import java.util.ArrayList;

public class House extends SharedValues {
    private String region;
    private String coatOfArms;
    private final ArrayList<String> swornMembers = new ArrayList<>();

    public House(String name, String region, String coatOfArms) {
        this.name = name;
        this.region = region;
        this.coatOfArms = coatOfArms;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCoatOfArms() {
        return coatOfArms;
    }

    public void setCoatOfArms(String coatOfArms) {
        this.coatOfArms = coatOfArms;
    }

    public ArrayList<String> getSwornMembers() {
        return swornMembers;
    }

    public void setSwornMembers(String swornMember) {
        this.swornMembers.add(swornMember);
    }

    @Override
    public String toString() {
        return "\n House {" +
                "name='" + name + '\'' +
                ", region='" + region + '\'' +
                ", coatOfArms='" + coatOfArms + '\'' +
                ", swornMembers=" + swornMembers +
                "} \n";
    }
}
