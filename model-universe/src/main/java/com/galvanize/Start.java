package com.galvanize;

import java.util.HashMap;

public class Start {

    public static void main(String[] args) {

        HashMap<House, Character> houseCharacterHashMap = new HashMap<>();

        House house = new House("Stark", "North", "Black Wolf");
        Character character = new Character("Jon Snow", "male", "Lord");

        houseCharacterHashMap.put(house, character);

        houseCharacterHashMap.forEach((key, value) -> {
            System.out.println(key + " " + value);
        });

    }

}
