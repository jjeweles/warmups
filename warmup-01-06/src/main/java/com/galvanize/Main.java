package com.galvanize;

public class Main {
    public static void main(String[] args) {

        printAllVowels("hey");
        printAllVowels("WhAtsUP");
        printAllVowels("howYOUdoin");

        System.out.println("\n\n");

        printAllVowelsShorter("testingTHEshorterMEthod");
        printAllVowelsShorter("all words are made up");

    }

    static void printAllVowels(String myString) {
        System.out.println("Vowels of Entered String: " + myString);
        myString = myString.toLowerCase();
        for (int i = 0; i < myString.length(); i ++) {
            if (myString.charAt(i) == 'a') {
                System.out.println(myString.charAt(i));
            } else if (myString.charAt(i) == 'e') {
                System.out.println(myString.charAt(i));
            } else if (myString.charAt(i) == 'i') {
                System.out.println(myString.charAt(i));
            } else if (myString.charAt(i) == 'o') {
                System.out.println(myString.charAt(i));
            } else if (myString.charAt(i) == 'u') {
                System.out.println(myString.charAt(i));
            }
        }
    }

    static void printAllVowelsShorter(String myString) {
        System.out.println("Vowels of Entered String: " + myString);
        myString = myString.toLowerCase();

        for (int i = 0; i < myString.length(); i++) {
            if (myString.charAt(i) == 'a'|| myString.charAt(i) == 'e'|| myString.charAt(i) == 'i' || myString.charAt(i) == 'o' || myString.charAt(i) == 'u') {
                System.out.println(myString.charAt(i));
            }
        }
    }
}