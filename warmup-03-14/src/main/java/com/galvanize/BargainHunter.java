package com.galvanize;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

public class BargainHunter {

    private final HashMap<String, ArrayList<Double>> bargainMap;

    public BargainHunter(HashMap<String, ArrayList<Double>> bargainMap) {
        this.bargainMap = bargainMap;
    }

    public void add(String item, Double price) {
        if (bargainMap.containsKey(item)) {
            bargainMap.get(item).add(price);
        } else {
            ArrayList<Double> prices = new ArrayList<>();
            prices.add(price);
            bargainMap.put(item, prices);
        }
    }

    private String formatDouble(Double value) {
        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        return decimalFormat.format(value);
    }

    private Double calculateAveragePrice(ArrayList<Double> prices) {
        Double sum = 0.0;
        for (Double price : prices) {
            sum += price;
        }
        return sum / prices.size();
    }

    public String getAveragePrice(String item) {
        ArrayList<Double> prices = bargainMap.get(item);
        Double average = calculateAveragePrice(prices);
        return formatDouble(average);
    }

    public HashMap<String, ArrayList<String>> getAllAveragePrices() {
        HashMap<String, ArrayList<String>> averagePrices = new HashMap<>();

        for (String item : bargainMap.keySet()) {
            ArrayList<Double> prices = bargainMap.get(item);
            Double average = calculateAveragePrice(prices);
            ArrayList<String> averagePrice = new ArrayList<>();
            averagePrice.add(formatDouble(average));
            averagePrices.put(item, averagePrice);
        }

        return averagePrices;
    }

}
