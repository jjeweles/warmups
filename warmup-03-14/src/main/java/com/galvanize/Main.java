package com.galvanize;

import java.util.ArrayList;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {

        HashMap<String, ArrayList<Double>> bargainMap = new HashMap<>();

        BargainHunter bargainHunter = new BargainHunter(bargainMap);

        bargainHunter.add("Oranges", 2.00);
        bargainHunter.add("Oranges", 1.50);
        bargainHunter.add("Oranges", 1.50);

        bargainHunter.add("Apples", 1.00);
        bargainHunter.add("Apples", 1.00);
        bargainHunter.add("Apples", 4.00);

        System.out.println(bargainHunter.getAveragePrice("Apples"));
        System.out.println(bargainHunter.getAveragePrice("Oranges"));

        System.out.println(bargainHunter.getAllAveragePrices());


    }
}