package com.galvanize;

public class Main {
    public static void main(String[] args) {
        Game game = new Game();

        System.out.println(game.playerOne);
        
        Game game2 = new Game("Justin");

        game2.setGameInProgress(true);
        System.out.println("Game in Progress One Player: " + game2.gameInProgress);

        Game game3 = new Game("Justin", "Mike");
        System.out.println("Game in Progress: " + game3.gameInProgress);

        Player pl1 = new Player("John");
        Player pl2 = new Player("Heffrey");

        Game game4 = new Game(pl1, pl2);

        System.out.println("Player Names From Class Player: " + game4.playerOne + " & " + game4.playerTwo);

    }
}