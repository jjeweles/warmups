package com.galvanize;

public class Game {

    String playerOne;
    String playerTwo;
    boolean gameInProgress;

    public Game() {
        this("Player 1", "Player 2");
    }

    public Game(String playerOne) {
//        this.playerOne = playerOne;
        this(playerOne, "Player 2");
        this.gameInProgress = false;
    }

    public Game(String playerOne, String playerTwo) {
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
        this.gameInProgress = true;
    }

    public Game(Player player1, Player player2) {
        this.playerOne = player1.name;
        this.playerTwo = player2.name;
    }

    public String getPlayerOne() {
        return playerOne;
    }

    public void setPlayerOne(String playerOne) {
        this.playerOne = playerOne;
    }

    public String getPlayerTwo() {
        return playerTwo;
    }

    public void setPlayerTwo(String playerTwo) {
        this.playerTwo = playerTwo;
    }

    public boolean getGameInProgress() {
        return gameInProgress;
    }

    public void setGameInProgress() {
        if (playerOne == null || playerTwo == null) {
            this.gameInProgress = false;
        } else {
            this.gameInProgress = true;
        }
    }
}
