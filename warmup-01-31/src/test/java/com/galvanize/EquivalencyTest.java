package com.galvanize;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EquivalencyTest<T> {

    Equivalency equivalency;

    @BeforeEach
    void setUp() {
        // Setup
        equivalency = new Equivalency();
    }
    
    @Test
    void equivalentTestWithIntsShouldReturnBooleanIfBothListsAreEquivalent() {
        // Setup
        ArrayList<Object> list1 = new ArrayList<>(Arrays.asList(1, 2, 3));
        ArrayList<Object> list2 = new ArrayList<>(Arrays.asList(3, 2, 1));
        ArrayList<Object> list3 = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        ArrayList<Object> list4 = new ArrayList<>(Arrays.asList(1, 1, 2, 3, 4, 4));
        
        // Enact Method
        // equivalency.isEquivalent();
        
        // Assert
         assertTrue(equivalency.isEquivalent(list1, list2));
         assertFalse(equivalency.isEquivalent(list1, list3));
         assertTrue(equivalency.isEquivalent(list3, list4));
        
        // Teardown
    }

    @Test
    void equivalentTestWithStringsShouldReturnBooleanIfBothListsAreEquivalent() {
        // Setup
        ArrayList<Object> list1 = new ArrayList<>(Arrays.asList("a", "b", "c"));
        ArrayList<Object> list2 = new ArrayList<>(Arrays.asList("c", "b", "a"));
        ArrayList<Object> list3 = new ArrayList<>(Arrays.asList("a", "b", "c", "d"));
        ArrayList<Object> list4 = new ArrayList<>(Arrays.asList("a", "b", "c", "d", "d"));

        // Assert
        assertTrue(equivalency.isEquivalent(list1, list2));
        assertFalse(equivalency.isEquivalent(list1, list3));
        assertTrue(equivalency.isEquivalent(list3, list4));

        // Teardown
    }
}
