package com.galvanize;

import java.awt.*;
import java.util.ArrayList;

public class Equivalency {

    public boolean isEquivalent(ArrayList<Object> list1, ArrayList<Object> list2) {

        return list1.containsAll(list2);

    }

}
