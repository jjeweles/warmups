package com.galvanize;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ArrayListsTest {

    @Test
    void sizeShouldReturnSizeOfArrayListWhenRan(){
        // Setup
        ArrayLists arr = new ArrayLists();
        ArrayList<Integer> nums = new ArrayList<>(Arrays.asList(1,2,3,4,5));

        // Enact - make method
        // arr.size(nums);
        nums.size();
        // Assert
        assertEquals(5, arr.size(nums));

        // Enact
        nums.add(6);
        // Assert
        assertEquals(6, arr.size(nums));
    }

    @Test
    void containsShouldReturnTrueIfPassedArgIsInArrayList(){
        // Setup
        ArrayLists arr = new ArrayLists();
        ArrayList<Integer> nums = new ArrayList<>(Arrays.asList(1,2,3,4,5));

        // Enact - make method
        // arr.contains();

        // Assert
        assertEquals(true, arr.contains(nums, 1));
        assertEquals(true, arr.contains(nums, 3));
        assertEquals(false, arr.contains(nums, 10));
    }

    @Test
    void getShouldReturnIndexOfElementIfItExistInArrayListOrNegOneIfItDoesNot(){
        // Setup
        ArrayLists arr = new ArrayLists();
        ArrayList<Integer> nums = new ArrayList<>(Arrays.asList(1,2,3,4,5));

        // Enact - method creation
        // arr.get(nums, 1);

        // Assert
        assertEquals(1, arr.get(nums, 2));
        assertEquals(-1, arr.get(nums, 7));
        assertEquals(0, arr.get(nums, 1));
    }

}
