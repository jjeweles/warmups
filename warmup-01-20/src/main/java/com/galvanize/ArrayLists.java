package com.galvanize;

import java.util.ArrayList;

public class ArrayLists {


    public int size(ArrayList arr) {
        int arrListSize = 0;
        // loop through array and add 1 for each element
        for (Object num : arr) {
            arrListSize += 1;
        }

        return arrListSize;
    }

    public boolean contains(ArrayList arr, int num){
        boolean result = false;

        // loop through arraylist and update boolean if list contains argument passed
        for (Object arrNum : arr) {
            if (arrNum.equals(num)) {
                result = true;
            }
        }

        return result;
    }

    public int get(ArrayList arr, int numToFind){
        int index = -1;
        int count = 0;
        // loop through arraylist and if numToFind arg is equal to an element in array
        // set local index var to indexOf element
        for (Object els : arr) {
            if (els.equals(numToFind)) {
                index = count;
            }
            count++;
        }

        return index;
    }

}
