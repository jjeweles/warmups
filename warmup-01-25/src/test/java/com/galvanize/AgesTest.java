package com.galvanize;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class AgesTest {

    Ages agesTestInstance;
    HashMap<String, Integer> expectedTestHashMap = new HashMap<>();

    @BeforeEach
    void setAges() {
        // Setup
        agesTestInstance = new Ages();
        expectedTestHashMap.put("Justin", 36);
        expectedTestHashMap.put("Robert", 37);
        expectedTestHashMap.put("Chris", 38);
    }

    @Test
    void addFriendShouldAddFriendAndAgeToHashMapMyFriendsAges() throws NameAlreadyExistsException {

        // Enact
        agesTestInstance.addFriend("Justin", 36);
        agesTestInstance.addFriend("Robert", 37);
        agesTestInstance.addFriend("Chris", 38);


        // Assert
        assertEquals(expectedTestHashMap, agesTestInstance.getMyFriendsAges());
        assertThrows(NameAlreadyExistsException.class, () -> {
            agesTestInstance.addFriend("Justin", 23);
        });
        // System.out.println(ages.getMyFriendsAges());

    }

    @Test
    void birthdayShouldAddOneYearToAgeOfGivenKeyInHashMap() throws Exception {
        // Setup
        agesTestInstance.addFriend("Justin", 36);
        agesTestInstance.addFriend("Robert", 37);
        agesTestInstance.addFriend("Chris", 38);

        // Enact
        agesTestInstance.birthday("Justin");
        agesTestInstance.birthday("Chris");
        agesTestInstance.birthday("Robert");
        expectedTestHashMap.put("Justin", expectedTestHashMap.get("Justin") + 1);
        expectedTestHashMap.put("Robert", expectedTestHashMap.get("Robert") + 1);
        expectedTestHashMap.put("Chris", expectedTestHashMap.get("Chris") + 1);


        // Assert
        assertEquals(expectedTestHashMap, agesTestInstance.getMyFriendsAges());
        assertThrows(Exception.class, () -> {
            agesTestInstance.birthday("Jeff");
        });
        // System.out.println(ages.getMyFriendsAges());
    }


}
