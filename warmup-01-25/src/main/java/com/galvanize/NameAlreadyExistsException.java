package com.galvanize;

public class NameAlreadyExistsException extends Exception {

    public NameAlreadyExistsException(String message) {
        super(message);
    }

}
