package com.galvanize;

import java.util.HashMap;

public class Ages {

    private HashMap<String, Integer> myFriendsAges = new HashMap<>();

    void addFriend(String name, int age) throws NameAlreadyExistsException {
        if (myFriendsAges.containsKey(name)) {
            throw new NameAlreadyExistsException("Name already exists in Map.");
        } else {
            myFriendsAges.put(name, age);
        }
    }

    void birthday(String name) throws Exception {
        try {
            myFriendsAges.put(name, myFriendsAges.get(name) + 1);
        } catch (Exception e) {
            throw new Exception("Name not in HashMap");
        }
    }

    HashMap<String, Integer> getMyFriendsAges(){
        return myFriendsAges;
    }

}
